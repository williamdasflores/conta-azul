package br.com.contaazul.billing.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataUtil {
	
	private static String FORMATO_YYYY_MM_DD = "yyyy-MM-dd";
	
	public static String formatarDateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_YYYY_MM_DD);
		return sdf.format(date);
	}
	

}
 