package br.com.contaazul.billing.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.contaazul.billing.domain.Billing;

@Consumes("application/json")
@Produces("application/json")
@Path("/bankslips")
public interface BillingService {
	
	@POST
	Response createBilling(Billing billing);
	
	@GET
	List<Billing> getBillings();
	
	@Path("/{id}")
	@GET
	Billing getBilling(@PathParam(value="id") String id);
	
	@Path("/{id}/payments")
	@POST
	Response payBilling(@PathParam(value="id") String id,  Billing billing);
	
	@Path("/{id}")
	@DELETE
	Response cancelBilling(@PathParam(value="id") String id);

}
