package br.com.contaazul.billing.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.contaazul.billing.domain.Billing;
import br.com.contaazul.billing.entity.BillingEntity;
import br.com.contaazul.billing.enums.StatusBillingEnum;
import br.com.contaazul.billing.repository.BillingRepository;
import br.com.contaazul.billing.validator.Validator;

@Service
public class BillingServiceImpl implements BillingService {
	
	@Autowired
	BillingRepository repository;
	
	@Autowired
	Validator validate;

	@Override
	public Response createBilling(Billing billing) {
		
		try {
			
			validate.validateCreateBilling(billing);
			
			BillingEntity entity = new BillingEntity(UUID.randomUUID().toString(),
					billing.getDueDate(),
					billing.getTotalInCents(),
					billing.getCustomer(),
					StatusBillingEnum.PENDING);
			
			repository.save(entity);
			
			Billing response = new Billing(entity.getId(),
					entity.getDueData(),
					entity.getTotalInCents(),
					entity.getCustomer(),
					entity.getStatus().toString());
				
			
			return Response.ok(response).build();
		} catch (BadRequestException e) {
			return Response.serverError().status(Response.Status.BAD_REQUEST.getStatusCode(),
					"Bankslip not provided in the request body").build();
		} catch (Exception e) {
			return Response.status(422, "Invalid bankslip provided. The possible reason are: "
					+ "A field of the provided bankslip was null or with invalid values").build();
		}
	}

	@Override
	public List<Billing> getBillings() {
		List<BillingEntity> listBilling = repository.findAll();
		
		List<Billing> billings = listBilling.stream().map(e -> new Billing(e.getId(),
				e.getDueData(),
				e.getTotalInCents(),
				e.getCustomer(), 
				e.getStatus().toString())).collect(Collectors.toList());
		
		return billings;
	}

	@Override
	public Billing getBilling(String id) {
		BillingEntity entity = repository.findById(id).get();

		if (entity == null) {
			throw new NotFoundException("Bankslip not found with the specific id");
		}
		
		return calculateTaxes(entity);
	}
	

	@Override
	public Response payBilling(String id, Billing billing) {
		
		if (repository.findById(id).get() == null) {
			throw new NotFoundException("Bankslip not found with the specific id");
		}
		
		BillingEntity entity = new BillingEntity();
		entity.setId(id);
		entity.setDueData(billing.getPaymentDate());
		entity.setStatus(StatusBillingEnum.PAID);
		
		repository.save(entity);
		
		return Response.ok().build();
	}

	@Override
	public Response cancelBilling(String id) {
		
		if (repository.findById(id).get() == null) {
			throw new NotFoundException("Bankslip not found with the specific id");
		}
		
		repository.deleteById(id);
		return Response.noContent().build();
	}
	
	private Billing calculateTaxes(BillingEntity entity) {
		Billing billing = new Billing();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(entity.getDueData());
		cal.add(Calendar.DATE, 10);
		
		if (entity.getDueData().after(cal.getTime())) {
			billing.setTotalInCents(entity.getTotalInCents().add(
					entity.getTotalInCents().multiply(new BigDecimal(1))));
		} else if (entity.getDueData().before(cal.getTime())) {
			billing.setTotalInCents(entity.getTotalInCents().add(
					entity.getTotalInCents().multiply(new BigDecimal(0.5))));
		} else {
			billing.setTotalInCents(entity.getTotalInCents());
		}
		
		billing.setId(entity.getId());
		billing.setDueDate(entity.getDueData());
		billing.setCustomer(entity.getCustomer());
		billing.setStatus(entity.getStatus().toString());
		
		return billing;
	}

}
