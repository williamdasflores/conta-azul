package br.com.contaazul.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import br.com.contaazul.billing.entity.BillingEntity;

@Component
public interface BillingRepository extends JpaRepository<BillingEntity, String>{

}
