package br.com.contaazul.billing.enums;

public enum StatusBillingEnum {
	PENDING,
	PAID,
	CANCELED;
}
