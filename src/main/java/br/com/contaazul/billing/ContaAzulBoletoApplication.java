package br.com.contaazul.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContaAzulBoletoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContaAzulBoletoApplication.class, args);
	}
}
