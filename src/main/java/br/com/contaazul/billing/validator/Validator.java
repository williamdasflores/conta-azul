package br.com.contaazul.billing.validator;

import javax.ws.rs.BadRequestException;

import org.springframework.stereotype.Component;

import br.com.contaazul.billing.domain.Billing;

@Component
public class Validator {
	
	public void validateCreateBilling(Billing  billing) throws BadRequestException{
		if (billing.getDueDate() == null || 
				billing.getTotalInCents() == null ||
				(billing.getCustomer() == null || "".equals(billing.getCustomer()))) {
			throw new BadRequestException();
		}
			
		
	}

}
