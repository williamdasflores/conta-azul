package br.com.contaazul.billing.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.contaazul.billing.enums.StatusBillingEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BillingEntity {
	
	@Id
	private String id;
	private Date dueData;
	private BigDecimal totalInCents;
	private String customer;
	private StatusBillingEnum status;
	
}
